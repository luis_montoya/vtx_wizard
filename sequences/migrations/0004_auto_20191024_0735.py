# Generated by Django 2.2.6 on 2019-10-24 12:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sequences', '0003_auto_20191023_1027'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testplan',
            name='rack_test',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='sequences.TestOrder'),
        ),
    ]
