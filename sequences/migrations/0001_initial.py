# Generated by Django 2.2.6 on 2019-10-21 14:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='RackTest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=255, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Step',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('case_number', models.IntegerField()),
                ('case_name', models.CharField(blank=True, max_length=255, null=True)),
                ('rack_test', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='rack_test', to='sequences.RackTest')),
            ],
        ),
        migrations.CreateModel(
            name='TestPlan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('step', models.IntegerField()),
                ('low_limit', models.FloatField()),
                ('expected_value', models.FloatField()),
                ('high_limit', models.FloatField()),
                ('units', models.CharField(blank=True, max_length=255, null=True)),
                ('type_format', models.CharField(blank=True, max_length=255, null=True)),
                ('flow_control', models.CharField(blank=True, max_length=255, null=True)),
                ('function_step', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='sequences.Step')),
                ('rack_test', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='sequences.RackTest')),
            ],
        ),
    ]
