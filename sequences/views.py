from django.shortcuts import render, redirect
from django.core.files.storage import FileSystemStorage
from django.views import View
from django.apps import apps

from .parser_files import *


def save_test_order(items, model):
    position = 0
    lst_test = []
    for key, value in items:
        if 'test' in key.split('_')[0]:
            position += 1
            test = RackTest.objects.get(name=value)
            lst_test.append(TestOrder(model=model, test=test, step=position))

    TestOrder.objects.bulk_create(
        lst_test
    )
    print(lst_test)

class ListSequencesView(View):

    def get(self, request):
        test_plan = TestPlan.objects.all().order_by('rack_test__step', 'step')

        return render(request, 'sequences.html', {'tests':test_plan})

    def post(self, request):
        pass


class SequenceView(View):

    def get(self, request):
        rack_test = RackTest.objects.all()
        sub_sequences = Step.objects.all()
        Model = apps.get_model('configurations', 'Model')
        models = Model.objects.all()

        return render(request, 'add_sequences.html', {'rack_test':rack_test, 'sub_sequences':sub_sequences, 'models':models})

    def post(self, request):
        perro = [x for x in request.FILES.getlist('myfile')]
        files = []
        model_selected = False

        rack_test = RackTest.objects.all()
        sub_sequences = Step.objects.all()
        Model = apps.get_model('configurations', 'Model')
        models = Model.objects.all()

        if len(perro) > 0:
            for f in perro:
                fs = FileSystemStorage()
                filename = fs.save(f.name, f)
                uploaded_file_url = fs.url(filename)
                files.append(uploaded_file_url)

            prototypes, sub_sequences = parser(files)
            upload_sequences(prototypes)
            upload_sub_sequences(sub_sequences)
        else:
            if request.POST['model'] != '0':
                pk_model = request.POST['model']
                model = Model.objects.get(pk=pk_model)
                model_selected = True
                test_name = None
                sub_position = 0
                lst_sub_test = []
                tmp_test = ''

                save_test_order(request.POST.items(), model)

                for key, value in request.POST.items():
                    if 'test' in key.split('_')[0]:
                        test_name = value
                    else:
                        if 'case_name' in key:
                            if tmp_test != test_name:
                                tmp_test = test_name
                                sub_position = 1
                            else:
                                sub_position += 1
                            rack = RackTest.objects.get(name=test_name)
                            test = TestOrder.objects.get(test=rack)
                            function_step = Step.objects.get(case_name=value, rack_test=rack)
                            lst_sub_test.append(
                                TestPlan(
                                    rack_test=test,
                                    function_step=function_step,
                                    step=sub_position,
                                    low_limit=0.0,
                                    expected_value=0.0,
                                    high_limit=0.0,
                                    units='',
                                    type_format='',
                                    flow_control='',
                                )
                            )

                TestPlan.objects.bulk_create(
                    lst_sub_test
                )
                print(lst_sub_test)

        rack_test = RackTest.objects.all()
        sub_sequences = Step.objects.all()
        if not model_selected and len(perro) == 0:
            return render(request, 'add_sequences.html',
                          {'rack_test':rack_test, 'sub_sequences':sub_sequences, 'models':models, 'error':'No model was selected'})
        #if model_selected and len(perro) == 0:
        #    return render(request, 'sequences.html', {'rack_test': rack_test, 'sub_sequences': sub_sequences, 'models': models})
        return redirect('list_sequences')


