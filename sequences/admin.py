from django.contrib import admin

from .models import *


admin.site.register(RackTest)
admin.site.register(Step)
admin.site.register(TestPlan)
admin.site.register(TestOrder)
