import os
from .models import *

def parser(files):
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    FILE_NAME = ['{}{}'.format(BASE_DIR, files[1]), '{}{}'.format(BASE_DIR, files[0])]
    found_block = False
    data_block = []
    data_prototype = []
    prototypes = []

    with open(FILE_NAME[0], 'r') as header_file:
        for line in header_file:
            if line.find('DB-BLOCK') >= 0 and not found_block:
                found_block = True
                data_block.clear()
            if line.find('DB-ENDBLOCK') >= 0:
                found_block = False

            if found_block and line.find('DB-BLOCK') < 0:
                data_block.append(line)

    for d in data_block:
        function_prototype = d.split(' ')[1].split('(')[0]
        prototypes.append(function_prototype)

    found_prototype = False
    list_sequences = []
    for prototype in prototypes:
        with open(FILE_NAME[1], 'r') as code_file:
            for line in code_file:
                if line.find(prototype) >= 0 and not found_prototype:
                    found_prototype = True
                    data_prototype.clear()
                if line.find('End Test') >= 0:
                    found_prototype = False

                if found_prototype:
                    if line.find('DB-NAME') >= 0:
                        sub = line.split('=')[1].replace('\n', '').split(',')
                        print('function => {} case number => {} sub sequence => {}'.format(prototype, sub[0], sub[1]))
                        list_sequences.append('{}|{}|{}'.format(prototype, sub[0].strip(), sub[1]))

    for f in FILE_NAME:
        os.remove(f)
    return prototypes, list_sequences


def upload_sequences(prototypes):
    new_prototypes = []
    for p in prototypes:
        if not RackTest.objects.filter(name=p).exists():
            new_prototypes.append(RackTest(name=p))
    RackTest.objects.bulk_create(
        new_prototypes
    )
    print(new_prototypes)


def upload_sub_sequences(sub_sequence):
    for s in sub_sequence:
        dog = s.split('|')
        rack_test = RackTest.objects.get(name=dog[0])
        if not Step.objects.filter(case_number=dog[1], case_name=dog[2], rack_test=rack_test).exists():
            print('new steps ===================== {} {} {}'.format(rack_test, dog[2], rack_test))
            step = Step(
                case_number = dog[1],
                case_name = dog[2],
                rack_test = rack_test,
            )
            step.save()
        else:
            Step.objects.filter(rack_test=rack_test.pk, case_number=dog[1], case_name=dog[2]).update(case_name=dog[2])
            print('exists steps ===================== {} {} {}'.format(rack_test.pk, dog[2], rack_test))
