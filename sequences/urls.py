from django.urls import path, include
from .views import SequenceView, ListSequencesView

urlpatterns = [
    path('', SequenceView.as_view(), name="sequences"),
    path('list-sequences', ListSequencesView.as_view(), name='list_sequences'),
]