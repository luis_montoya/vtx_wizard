from django.db import models


class RackTest(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name


class Step(models.Model):
    case_number = models.IntegerField()
    case_name = models.CharField(max_length=255, blank=True, null=True)
    rack_test = models.ForeignKey(RackTest, on_delete=models.DO_NOTHING, related_name='rack_test')

    def __str__(self):
        return '{} ({})'.format(self.case_name, self.rack_test.name)



class TestOrder(models.Model):
    model = models.ForeignKey('configurations.Model', on_delete=models.DO_NOTHING)
    test = models.ForeignKey(RackTest, on_delete=models.DO_NOTHING)
    step = models.IntegerField()

    def __str__(self):
        return '{} {} step:({})'.format(self.model.name, self.test.name, self.step)


class TestPlan(models.Model):
    rack_test = models.ForeignKey(TestOrder, on_delete=models.DO_NOTHING)
    function_step = models.ForeignKey(Step, on_delete=models.DO_NOTHING)
    step = models.IntegerField()
    low_limit = models.FloatField()
    expected_value = models.FloatField()
    high_limit = models.FloatField()
    units = models.CharField(max_length=255, blank=True, null=True)
    type_format = models.CharField(max_length=255, blank=True, null=True)
    flow_control = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return '{} {} {}'.format(self.rack_test.test, self.function_step.case_name, self.step)

