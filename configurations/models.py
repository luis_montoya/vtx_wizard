from django.db import models


class Location(models.Model):
    descriptions = (
        ('Rack', 'Rack'),
        ('Fixture', 'Fixture'),
        ('Nest', 'Nest'),
    )
    name = models.CharField(max_length=255, choices=descriptions, default='Rack')
    location_id = models.IntegerField()

    def __str__(self):
        return self.name


class Fixture(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    nest_number =  models.IntegerField()
    side = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name


class Model(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    part_number = models.CharField(max_length=255, blank=True, null=True)
    fixture = models.ForeignKey(Fixture, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.name
