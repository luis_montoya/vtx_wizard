# Django
from django.contrib import admin

# Models
from .models import *

admin.site.register(Fixture)
admin.site.register(Model)
admin.site.register(Location)
