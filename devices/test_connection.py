#!/usr/bin/env python3

import socket
import serial
import visa
from pyModbusTCP.client import ModbusClient

class TestConnection:

    def __init__(self, configurations):

        if configurations['device_type'] == 'TCP' or configurations['device_type'] == 'MODBUS':
            self.host = configurations['device_conf1']
            self.port = configurations['device_conf2']
        elif configurations['device_type'] == 'RS232':
            self.com = configurations['device_conf1']
            self.baud_rate = configurations['device_conf2']
            self.parity = configurations['device_conf3']
            self.data_bits = configurations['device_conf4']
            self.stop_bits = configurations['device_conf5']
        elif configurations['device_type'] == 'VISA':
            self.instrument = configurations['device_conf1']

    def tcp_connection(self):
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((self.host, int(self.port)))
            s.close()
            return 'Connected'
        except:
            return 'Not Connected'


    def serial_connection(self):
        try:
            ser = serial.Serial(self.com)
            ser.close()
            return 'Connected'
        except:
            return 'Not Connected'

    def modbus_communication(self):
        try:
            c = ModbusClient(host=self.host, port=int(self.port))
            c.open()
            if c.is_open():
                c.close()
                return 'Connected'
            return 'Not Connected'
        except:
            return 'Not Connected'

    def visa_communication(self):
        try:
            rm = visa.ResourceManager()
            instrument = rm.open_resource(self.instrument)
            identity = instrument.query("*IDN?")
            return 'Connected'
        except:
            return 'Not Connected'
