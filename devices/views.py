# Django
from django.shortcuts import render, redirect
from django.views import View
from .test_connection import TestConnection
from django.apps import apps

# Models
from .models import *


class DevicesView(View):

    def get(self, request, *args, **kwargs):
        templates = DeviceTemplate.objects.all()
        devices = Device.objects.all()
        Fixture = apps.get_model('configurations', 'Fixture')
        Location = apps.get_model('configurations', 'Location')
        locations = Location.objects.all()
        fixtures = Fixture.objects.all()
        test_configurations = {}
        connected = 'none'
        primary = -1

        if len(request.GET) > 0:
            for key, value in request.GET.items():
                test_configurations[key] = value
            test_connection = TestConnection(test_configurations)
            primary = int(request.GET['device_pk'])
            if request.GET['device_type'] == 'TCP':
                connected = test_connection.tcp_connection()
            elif request.GET['device_type'] == 'RS232':
                connected = test_connection.serial_connection()
            elif request.GET['device_type'] == 'MODBUS':
                connected = test_connection.modbus_communication()
            elif request.GET['device_type'] == 'VISA':
                connected = test_connection.visa_communication()
        else:
            connected = 'none'


        return render(request, 'devices.html', {'templates':templates, 'fixtures':fixtures, 'devices':devices, 'connected':connected, 'primary':primary, 'locations':locations})

    def post(self, request, *args, **kwargs):
        templates = DeviceTemplate.objects.all()
        Fixture = apps.get_model('configurations', 'Fixture')
        Location = apps.get_model('configurations', 'Location')
        locations = Location.objects.all()
        fixtures = Fixture.objects.all()
        device = Device()
        response = 'ok'
        all_rack = False

        for key, value in request.POST.items():
            print('key == {} value == {}'.format(key, value))
            if 'title' in key:
                template = DeviceTemplate.objects.get(pk=value)
                device.template = template
            elif 'fixture' in key:
                fixture = Fixture.objects.get(pk=value)
                device.fixture = fixture
            elif 'description' in key:
                device.configuration = value
            elif 'configuration' in key:
                conf = key.split('_')
                if conf[0] == 'configuration':
                    device.configuration1 = value
                elif conf[0] == 'configuration1':
                    device.configuration2 = value
                elif conf[0] == 'configuration2':
                    device.configuration3 = value
                elif conf[0] == 'configuration3':
                    device.configuration4 = value
                elif conf[0] == 'configuration4':
                    device.configuration5 = value
            elif 'location' in key:
                if value == '-1':
                    device.location = value
                    all_rack = True
            elif 'nest' in key and not all_rack:
                if value == 'all':
                    device.location = '-2'
                else:
                    device.location = value
        try:
            device.save()
        except:
            response = 'nok'
        devices = Device.objects.all()


        return render(request, 'devices.html', {'templates':templates, 'fixtures':fixtures, 'devices':devices, 'response':response, 'locations':locations})
