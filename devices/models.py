from django.db import models


class DeviceType(models.Model):
    device_type = models.CharField(max_length=255, blank=True, null=True)
    configuration = models.CharField(max_length=255, blank=True, null=True)
    configuration1 = models.CharField(max_length=255, blank=True, null=True)
    configuration2 = models.CharField(max_length=255, blank=True, null=True)
    configuration3 = models.CharField(max_length=255, blank=True, null=True)
    configuration4 = models.CharField(max_length=255, blank=True, null=True)
    configuration5 = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.device_type


class DeviceTemplate(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    last_update = models.DateTimeField(auto_now=True)
    image = models.ImageField(upload_to='uploads', blank=True)
    type = models.ForeignKey(DeviceType, on_delete=models.DO_NOTHING)

    def __str__(self):
        return '{} {}'.format(self.name, self.type)


class Device(models.Model):
    location = models.IntegerField()
    template = models.ForeignKey(DeviceTemplate, on_delete=models.DO_NOTHING)
    fixture = models.ForeignKey('configurations.Fixture', on_delete=models.DO_NOTHING)
    configuration = models.CharField(max_length=255, blank=True, null=True)
    configuration1 = models.CharField(max_length=255, blank=True, null=True)
    configuration2 = models.CharField(max_length=255, blank=True, null=True)
    configuration3 = models.CharField(max_length=255, blank=True, null=True)
    configuration4 = models.CharField(max_length=255, blank=True, null=True)
    configuration5 = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return '{} {} ({})'.format(self.template, self.configuration, self.fixture)
