from django.urls import path, include
from .views import DevicesView

urlpatterns = [
    path('', DevicesView.as_view(), name='devices'),
]