# pull oficial base image
FROM python:3.7.4

# set work directory
WORKDIR /src/app

# set enviroment variables
ENV PYTHONWRITEBYTECODE 1
ENV PYTHONBUFFERED 1

# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt /src/app/requirements.txt
RUN pip install -r requirements.txt

# copy project
COPY . /src/app/
