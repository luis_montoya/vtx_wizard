from django.contrib import admin

from .models import LogFileDetail, LogFile

admin.site.register(LogFile)
admin.site.register(LogFileDetail)
