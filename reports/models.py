from django.db import models


class LogFileDetail(models.Model):
    serial_number = models.CharField(max_length=255, blank=True, null=True)
    model = models.CharField(max_length=255, blank=True, null=True)
    test = models.CharField(max_length=255, blank=True, null=True)
    sub_function = models.CharField(max_length=255, blank=True, null=True)
    low_limit = models.CharField(max_length=255, blank=True, null=True)
    expected_value = models.CharField(max_length=255, blank=True, null=True)
    high_limit = models.CharField(max_length=255, blank=True, null=True)
    units = models.CharField(max_length=255, blank=True, null=True)
    format = models.CharField(max_length=255, blank=True, null=True)
    status = models.CharField(max_length=255, blank=True, null=True)
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} {} {}'.format(self.serial_number, self.model, self.status)


class LogFile(models.Model):
    serial_number = models.CharField(max_length=255, blank=True, null=True)
    model = models.CharField(max_length=255, blank=True, null=True)
    status = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return '{} {} {}'.format(self.serial_number, self.model, self.status)