from django.db import models


class User(models.Model):
    username = models.CharField(max_length=255, blank=True, null=True)
    password = models.CharField(max_length=255, blank=True, null=True)
    level = models.IntegerField()
    last_access = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.username
